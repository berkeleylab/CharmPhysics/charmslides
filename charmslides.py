#!/usr/bin/env python
import os
import sys
import tempfile
import shutil
import subprocess
import yaml

SOURCE_DIR = os.path.dirname(os.path.realpath(__file__))


def run(options, args):

    # config file
    config = {}
    with open(os.path.join(SOURCE_DIR, 'tex', 'config', '%s.yaml' % options.config), 'r') as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            config = exc

    # path to plots
    plots_path = os.path.abspath(options.plots_path)

    # make a temp build ri
    temp_dir = tempfile.mkdtemp()
    os.environ['CHARM_TEX_PATH'] = temp_dir

    # copy latex template to temp dir
    shutil.copyfile(os.path.join(SOURCE_DIR, 'tex', 'template.tex'),
                    os.path.join(temp_dir, 'template.tex'))
    shutil.copyfile(os.path.join(SOURCE_DIR, 'tex', 'Makefile'),
                    os.path.join(temp_dir, 'Makefile'))

    # make input 'hook' file
    f = open(os.path.join(temp_dir, 'hook.tex'), 'w')

    # plot name
    name_template = config['name_template']

    # sections
    sections = config['sections']

    for section in sections:

        items = config['items']

        f.write("\section{%s}\n" % section.replace("_", "\_"))

        for subsection in config['subsections']:

            # 3x2 configuration
            if len(items) == 6:
                width = 0.27
                items_per_line = 3
            # 2x1 configuration
            elif len(items) <= 2:
                width = 0.5
                items_per_line = 2
            # unknown configuration
            else:
                print ("unsupported item length")
                sys.exit(1)
            width = min(0.38, width)

            f.write("\subsection{%s}\n" % subsection.replace("_", "\_"))
            f.write("\\begin{frame}{%s %s}\n" %
                    (section.replace("_", "\_"), subsection.replace("_", "\_")))
            f.write("\\vspace{-12pt}\n")
            f.write("\\begin{figure}[!ht]\n")

            for i, item in enumerate(items):

                # generate plot name
                plot_name = name_template.replace("<section>", section).replace("<subsection>", subsection).replace("<item>", item)

                # includegraphics
                plots_string = "\t\subfloat[]{\includegraphics[width=%s\columnwidth]{%s/%s}}\n" % (width, plots_path, plot_name)

                # write
                f.write(plots_string)

                # add new line if needed
                if i > 0 and (i+1) != len(config['items']) and (i+1) % items_per_line == 0:
                    f.write("\\\\\\vspace{-26pt}\n")

            f.write("\end{figure}\n")
            f.write("\end{frame}\n\n")

    # close the 'hook' file
    f.close()

    # pdflatex
    c = subprocess.call(["make"], cwd=temp_dir)

    print (temp_dir)

    # copy pdf file
    shutil.copyfile(os.path.join(temp_dir, 'template.pdf'),
                    os.path.join(os.getcwd(), '%s.pdf' % options.output_name))


if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser()

    # ----------------------------------------------------
    # required
    # ----------------------------------------------------
    parser.add_option('-o', '--output-name',
                      action="store", dest="output_name",
                      help="output name (default: charmslides)",
                      default="charmslides")
    parser.add_option('-i', '--plots-path',
                      action="store", dest="plots_path",
                      help="path to the plots")
    parser.add_option('-a', '--config',
                      action="store", dest="config",
                      help="configuration file")

    # parse input arguments
    options, args = parser.parse_args()

    run(options, args)
